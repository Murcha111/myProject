import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import  webpack from 'webpack';
import { BuildOptions } from './types/config';

export function buildLoaders({isDev}: BuildOptions):webpack.RuleSetRule[]{
    const cssLoader={
        test: /\.s[ac]ss$/i,
        use: [
          // отделяет css файлы в build в папку css вместо того, чтобы билдить стили вместе с js 
        isDev? 'style-loader' :MiniCssExtractPlugin.loader,
          // Translates CSS into CommonJS
          {loader: "css-loader",
          options: {
            modules: {
              auto: (resPath: string) =>Boolean(resPath.includes('.module')),
              localIdentName: isDev ?
                                   "[path][name]__[local]--[hash:base64:5]"
                                    :
                                   "[hash:base64:5]" ,
            }
         
          }},
          // Compiles Sass to CSS
          "sass-loader",
        ],
      }
    //елси нет тайпскрипта, нужен babel-loader
const typescriptLoader={
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      
}
    return  [
        typescriptLoader,
        cssLoader
    ]
}