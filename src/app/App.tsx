import { Suspense } from "react";
import "./styles/index.scss";
import { Route, Routes } from "react-router-dom";
import { Link } from "react-router-dom";

import { useTheme } from "app/providers/ThemeProvider";
import { AboutPageAsync } from "pages/AboutPage";
import { MainPageAsync } from "pages/MainPage";
import { classNames } from "shared/lib/classNames/classNames";
import { AppRouter } from "./providers/ThemeProvider/router";
// import { useTheme } from "providers/ThemeProvider";

export default function App() {
  //используем тему из хука useTheme и сохраняется она в локалсторадж
  const { theme, toogleTheme } = useTheme();
  //AboutPageAsync & MainPageAsync при сборке создают отдельные чанки ( lazy loading), чтобы подгружать только нгужные страницы, а не все сразу
  return (
    <div className={classNames("app", {}, [theme])}>
      <Link to={"/"}>Главная</Link>
      <Link to={"/about"}>О сатйе</Link>
      <AppRouter />
      <button onClick={toogleTheme}>toogle</button>
    </div>
  );
}
