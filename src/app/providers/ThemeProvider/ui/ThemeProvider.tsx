import React, { FC, useMemo, useState } from "react";
import {
  LOCAL_STORAGE_THEME_KEY,
  ThemeContext,
  Theme,
} from "app/providers/ThemeProvider/lib/ThemeContext";

const defaultTheme =
  (localStorage.getItem(LOCAL_STORAGE_THEME_KEY) as Theme) || Theme.LIGHT; //светлая по умолчанию, если ключ пуст
//оборачиваем необходимые компонеты в провайдер (children)
const ThemeProvider: FC = ({ children }) => {
  //сохраняем тему в локалсорадж
  const [theme, setTheme] = useState<Theme>(defaultTheme);

  //запоминаем не создавать новый каждый раз объект, а возвращать то, что есть(если не было из-ний)
  const defaultProps = useMemo(
    () => ({
      theme: theme,
      setTheme: setTheme,
    }),
    [theme]
  );
  return (
    <ThemeContext.Provider value={defaultProps}>
      {children}
    </ThemeContext.Provider>
  );
};
export default ThemeProvider;
